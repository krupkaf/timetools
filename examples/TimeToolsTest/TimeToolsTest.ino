#include "TimeTools.h"


void setup() {
        Serial.begin(115200);
}

void check(char b) {
        if (b) {
                Serial.println(F(" OK"));
        } else {
                Serial.println(F(" Error"));
        }
}
int8_t cmptm(struct tm *a, struct tm *b) {
        if (a->tm_year != b->tm_year) return 0;
        if (a->tm_mon != b->tm_mon) return 0;
        if (a->tm_mday != b->tm_mday) return 0;
        if (a->tm_hour != b->tm_hour) return 0;
        if (a->tm_min != b->tm_min) return 0;
        if (a->tm_sec != b->tm_sec) return 0;
        if (a->tm_wday != b->tm_wday) return 0;
        if (a->tm_yday != b->tm_yday) return 0;
        return 1;
}

void  printmt(struct tm *a) {
        Serial.print(a->tm_year + YEAR0);
        Serial.print('-');
        Serial.print(a->tm_mon + 1);
        Serial.print('-');
        Serial.print(a->tm_mday);
        Serial.print(' ');
        Serial.print(a->tm_hour);
        Serial.print(':');
        Serial.print(a->tm_min);
        Serial.print(':');
        Serial.print(a->tm_sec);
        Serial.print(' ');
        Serial.print('w');
        Serial.print(a->tm_wday);
        Serial.print(' ');
        Serial.print('y');
        Serial.print(a->tm_yday);
}

void test(uint8_t number, struct tm *a, time_t t) {
        Serial.print(number);
        Serial.print(':');

        time_t b;
        struct tm aa;

        b = mktime(a);
        printmt(a);
        Serial.print(F(" ~ "));
        Serial.print(b);
        check((b == t));

        gmtime(b, &aa);
        Serial.print(b);
        Serial.print(F(" ~ "));
        printmt(&aa);
        check((cmptm(a, &aa) == 1));
}

void test2(time_t a) {
        time_t b;
        struct tm aa;

        gmtime(a, &aa);
        b = mktime(&aa);

        Serial.print(a);
        Serial.print(F(" ~ "));
        printmt(&aa);
        Serial.print(F(" ~ "));
        Serial.print(b);
        check((a == b));
}

void loop() {
        struct tm a;

        Serial.println();
        Serial.println();
        Serial.println(F("TimeToolsTest"));
        Serial.println();

        a.tm_sec = 00;
        a.tm_min = 00;
        a.tm_hour = 00;
        a.tm_mday = 1;
        a.tm_mon = 1 - 1;
        a.tm_year = 0;
        a.tm_wday = 6;
        a.tm_yday = 0;
        test(1, &a, (946684800UL - UNIX0));

        a.tm_sec = 25;
        a.tm_min = 36;
        a.tm_hour = 23;
        a.tm_mday = 31;
        a.tm_mon = 12 - 1;
        a.tm_year = 16;
        a.tm_wday = 6;
        a.tm_yday = 365;
        test(2, &a, (1483227385 - UNIX0));

        a.tm_sec = 43;
        a.tm_min = 56;
        a.tm_hour = 13;
        a.tm_mday = 10;
        a.tm_mon = 2 - 1;
        a.tm_year = 19;
        a.tm_wday = 0;
        a.tm_yday = 40;
        test(3, &a, (1549807003 - UNIX0));

        a.tm_sec = 53;
        a.tm_min = 50;
        a.tm_hour = 12;
        a.tm_mday = 2;
        a.tm_mon = 6 - 1;
        a.tm_year = 19;
        a.tm_wday = 0;
        a.tm_yday = 152;
        test(4, &a, (1559479853 - UNIX0));

        Serial.println('*');

        test2(0);

        test2(12345678);

        test2(610124700);

        test2(626023000);

        test2(599759450);

        delay(2000);
}
