#include "TimeTools.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

a_time_t a;
struct a_tm atm;
time_t b;
struct tm *btm;
a_time_t c;

void print(void) {
  printf("%10u: %8d-%02d-%02d %02d:%02d:%02d", a, atm.tm_year + EPOCH_YR,
         atm.tm_mon + 1, atm.tm_mday, atm.tm_hour, atm.tm_min, atm.tm_sec);
  printf(" - %8d-%02d-%02d %02d:%02d:%02d", btm->tm_year + 1900,
         btm->tm_mon + 1, btm->tm_mday, btm->tm_hour, btm->tm_min, btm->tm_sec);
  printf("  %10u\n", c);
}

void main(void) {
  for (a = 0; a < 4294900000UL; a += 100) {
    b = a + UNIX0;
    a_gmtime(a, &atm);
    btm = gmtime(&b);
    c = a_mktime(&atm);

    if ((btm->tm_sec != atm.tm_sec) || (btm->tm_min != atm.tm_min) ||
        (btm->tm_hour != atm.tm_hour) || (btm->tm_mday != atm.tm_mday) ||
        (btm->tm_mon != atm.tm_mon) || (btm->tm_year != (atm.tm_year + 100)) ||
        (btm->tm_wday != atm.tm_wday) || (btm->tm_yday != atm.tm_yday) ||
        (btm->tm_isdst != atm.tm_isdst) || (a != c)) {
      printf("Error\n");
      print();
      exit(0);
    }

    if ((a % 100000000) == 0) {
      print();
    }
  }
  printf("OK\n");
}
