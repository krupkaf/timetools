#include "TimeTools.h"
#ifdef linux
#else
#include <avr/pgmspace.h>
#endif

#ifdef linux
void a_gmtime(a_time_t t, struct a_tm *tmbuf) {
#else
void gmtime(time_t t, struct tm *tmbuf) {
#endif
  int32_t tt;
  int8_t x, d;

  tmbuf->tm_sec = t % 60;
  t /= 60;
  tmbuf->tm_min = t % 60;
  t /= 60;
  tmbuf->tm_hour = t % 24;
  tt = t / 24;
  tmbuf->tm_wday = (tt + WDAY0) % 7;

  x = tt / 365;
  if (x > 100) { // Rok 2100 neni prestupny
    tt++;
  }
  tt %= 365;
  if (x % 4 == 0) {
    tt++;
  };
  tt -= 1 + (x / 4); // Odecteny 1 dne za kazdy prestupny rok
  if (tt < 0) {
    tt += 365;
    x--;
    if (x % 4 == 0) {
      tt++;
    };
  };
  tmbuf->tm_year = x;
  tmbuf->tm_yday = tt;

  for (x = 0;; x++) { // 0 - leden
    switch (x) {
    case 0:
    case 2:
    case 4:
    case 6:
    case 7:
    case 9:
    case 11:
      d = 31;
      break;
    case 1:
      d = 28;
      if (((tmbuf->tm_year % 4) == 0) && (tmbuf->tm_year != 100))
        d++;
      break;
    case 3:
    case 5:
    case 8:
    case 10:
    default:
      d = 30;
      break;
    };
    if (tt < d)
      break;
    tt -= d;
  };
  tmbuf->tm_mon = x;
  tmbuf->tm_mday = tt + 1;
};

#ifdef linux
a_time_t a_mktime(struct a_tm *tmbuf) {
  a_time_t seconds;
#else
time_t mktime(struct tm *tmbuf) {
  time_t seconds;
#endif
  seconds = tmbuf->tm_year * 365;      // Number of deys
  seconds += 1 + (tmbuf->tm_year / 4); // Add 1 dey for each leap year
  if (((tmbuf->tm_year % 4) == 0) && (tmbuf->tm_mon < 2)) {
    seconds--; // tm_year is leap year and tm_mon <= january
  }
  for (uint8_t i = 1; (i <= (tmbuf->tm_mon)) && (i <= 12); i++) {
    switch (i) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      seconds += 31;
      break;
    case 2:
      seconds += 28;
      if (tmbuf->tm_year == 100) {
        seconds -= 1;
      }
      break;
    case 4:
    case 6:
    case 9:
    case 11:
      seconds += 30;
      break;
    };
  };
  seconds += tmbuf->tm_mday - 1; // Number of all deys
  seconds *= 24;
  seconds += tmbuf->tm_hour; // Number of hour
  seconds *= 60;
  seconds += tmbuf->tm_min; // Number of mins
  seconds *= 60;
  seconds += tmbuf->tm_sec;

  return seconds;
}
