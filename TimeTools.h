#ifndef _TIMETOOLS_H
#define _TIMETOOLS_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _TIME_T_DEFINED
#define _TIME_T_DEFINED
#ifdef linux
typedef uint32_t a_time_t;
#else
typedef uint32_t time_t;
#endif
#endif

#define WDAY0 6 // Number of wday of 1.1. YEAR0
#define YEAR0 2000
#define EPOCH_YR 2000 // This two value is NOT posible to change.
#define UNIX0 946684800UL //Unix timestemp of 1.1.2000 00:00:00

#ifdef linux
struct a_tm {
#else
struct tm {
#endif
  uint8_t tm_sec;   // Seconds after the minute [0, 59]
  uint8_t tm_min;   // Minutes after the hour [0, 59]
  uint8_t tm_hour;  // Hours since midnight [0, 23]
  uint8_t tm_mday;  // Day of the month [1, 31]
  uint8_t tm_mon;   // Months since January [0, 11]
  int8_t tm_year;  // Years since YEAR0
  uint8_t tm_wday;  // Days since Sunday [0, 6]
  uint16_t tm_yday; // Days since January 1 [0, 365]
  uint8_t tm_isdst; // Daylight Saving Time flag
                          //!!!The values must be in the defined range!!!
};

/**
 * Convert sturct tm to time_t
 */
#ifdef linux
a_time_t a_mktime(struct a_tm *tmbuf);
#else
time_t mktime(struct tm *tmbuf);
#endif
/**
 * Convert time_t to tm struct
 */
#ifdef linux
void a_gmtime(a_time_t t, struct a_tm *tmbuf);
#else
void gmtime(time_t t, struct tm *tmbuf);
#endif

#ifdef __cplusplus
}
#endif

#endif /* _TIMETOOLS_H */
